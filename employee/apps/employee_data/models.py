from django.db import models
from model_utils.models import TimeStampedModel


class Employee(TimeStampedModel):
    name = models.CharField(max_length=255)
    email = models.CharField(max_length=50)
    department = models.CharField(max_length=30)

    def __str__(self):
        return f'{self.name} - {self.email}'
