import pytest

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from apps.employee_data.models import Employee

pytestmark = pytest.mark.django_db


def test_get_employees(new_employee):
    url = reverse('list-employee')
    client = APIClient()
    response = client.get(url)
    assert response.status_code == status.HTTP_200_OK
    employee = response.data[0]
    assert employee['name'] == 'Emeneu Cajumentício das Dores Conjugais'
    assert employee['email'] == 'emeneu@cajumenticio.com.br'
    assert employee['department'] == 'Sales'


def test_get_non_existent_employee(new_employee):
    url = reverse('list-employee', args=[1111])
    client = APIClient()
    response = client.get(url)
    assert response.status_code == status.HTTP_404_NOT_FOUND


def test_post_employee(new_employee):
    url = reverse('list-employee')
    client = APIClient()
    new_employee_data = {'name': 'Adler Medrado', 'email': 'adler@adlermedrado.com.br', 'department': 'Development'}
    response = client.post(url, new_employee_data)
    assert response.status_code == status.HTTP_201_CREATED


def test_post_invalid_employee():
    url = reverse('list-employee')
    client = APIClient()
    new_employee_data = {'name': 'Adler Medrado', 'email': 'adler@adlermedradotestetestetestetestetestetestetesteteste.com.br'}
    response = client.post(url, new_employee_data)
    assert response.status_code == status.HTTP_400_BAD_REQUEST


def test_put_invalid_employee(new_employee):
    employee = Employee.objects.filter(email='emeneu@cajumenticio.com.br')[0]
    url = reverse('detail-employee', args=[employee.id])
    client = APIClient()
    new_employee_data = {}
    response = client.put(url, new_employee_data)
    assert response.status_code == status.HTTP_400_BAD_REQUEST


def test_invalid_method():
    url = reverse('list-employee')
    client = APIClient()
    response = client.put(url, {})
    assert response.status_code == status.HTTP_405_METHOD_NOT_ALLOWED


def test_put_employee(new_employee):
    employee = Employee.objects.filter(email='emeneu@cajumenticio.com.br')[0]
    client = APIClient()
    url = reverse('detail-employee', args=[employee.id])
    new_employee_data = {'name': 'Adler Medrado', 'email': 'adler@adlermedrado.com.br', 'department': 'Development'}
    response = client.put(url, new_employee_data)
    employee = Employee.objects.get(pk=employee.id)
    employee.name == 'Adler Medrado'
    assert response.status_code == status.HTTP_200_OK


def test_patch_employee(new_employee):
    employee = Employee.objects.filter(email='emeneu@cajumenticio.com.br')[0]
    client = APIClient()
    url = reverse('detail-employee', args=[employee.id])
    new_employee_data = {'email': 'emeneu@yahoo.com'}
    response = client.patch(url, new_employee_data)
    employee = Employee.objects.get(pk=employee.id)
    employee.name == 'Emeneu Cajumentício das Dores Conjugais'
    employee.email == 'emeneu@yahoo.com'
    assert response.status_code == status.HTTP_200_OK


def test_delete_employee(new_employee):
    employee = Employee.objects.filter(email='emeneu@cajumenticio.com.br')[0]
    client = APIClient()
    url = reverse('detail-employee', args=[employee.id])
    response = client.delete(url, format='json')
    assert response.status_code == status.HTTP_204_NO_CONTENT
